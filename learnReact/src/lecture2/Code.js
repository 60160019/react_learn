import React, { Component } from 'react'
class Code extends Component {
    onCouponUse = (event) =>{
        this.props.setCoupon();
    }

    render() {
        const status = this.props.data.status;
        const coupon = this.props.data.code;
        return(
            <div>
                <div>
                    <button disabled={!status} onClick={this.onCouponUse}>Use Coupon</button>
                </div>
                <div>
                    <span>Coupon Code :</span>
                    <span>{status?coupon:'This Coupng Already used'}</span>
                </div>
            </div>
        )
    }
}

export default Code