import React, { Component } from 'react';
import Code from './Code'
class AdvenceCoupong extends Component {
  state = {
    coupon1: {
      code: 'b1912cf',
      status: true
    },
    coupon2: {
      code: 'bf45hg',
      status: true
    }
  };

  useCoupon = name => {
    const updatedCoupon = { ...this.state };
    updatedCoupon[name].status = false;
    this.setState(updatedCoupon);
  };
  render() {
    return (
      <div>
        <Code
          data={this.state['coupon1']}
          setCoupon={this.useCoupon.bind(this, 'coupon1')}
        />
        <Code
          data={this.state['coupon2']}
          setCoupon={this.useCoupon.bind(this, 'coupon2')}
        />
      </div>
    );
  }
}

export default AdvenceCoupong;
