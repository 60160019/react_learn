import React from 'react';
import logo from './logo.svg';
import '/App.css';

import Kuy from './Kuy';
import C1 from './C1';
import C2 from './C2';
import Post from './Post'
import Comment from './Comment'

function App() {
  const myname = "my is KUY jack"
  const n1 = 1
  const n2 = 1
  const aya = (
    <p>hi do mo watashiwa namaewa aya desu</p>
  );
  return (
    <div className="App">
    <div class="normal class kuy jack">

      </div>
      <p>Kuy Jack</p>
      {/* self tag closing for React */}
      <br/>
      {/* html for */}
      <label htmlFor="Kuy jack label">KUY</label>
      {/* used Assign variable */}
      <p>{myname}</p>
      {/* Use Expression */}
      <p>this is a sumation of plus {(n1+n2)+1}</p>
      {/* Short if */}
      {
        n1+n2 > 3?
        <p>Your good</p>:
        <p>Your Suck like who name Jack</p>
      }
      {aya}
      {/* Create Class Componet */}
      <Kuy/>
      {/* Child Component */}
      <C1/>
      <C2/>
      {/*  Set valueProps  and send Multiple props to child*/}
      <Post name="jack zakung" knowledge="low"/>
      <Post name="Bekzakung" knowledge="Very High"/>
      {/* Sub compoent */}
      <Post name="aya" knowledge="normal"><b>Sub component 1</b></Post>
      <Post name="maya" knowledge="normal"><h3>Sub component 2</h3></Post>
      <Post name="kata" knowledge="normal"><Comment data="it suck"/> </Post>
    </div>
    // React.createElement('div',{className : 'App'},React.createElement('p',null,'Kuyjack in element'))


  );
}

export default App;