import React from 'react'
const Post = (props) =>{
    const myName = props.name;
    const knowledge = props.knowledge
    return(
        <div>
            <p>Content from {myName} he int is {knowledge} {props.children}</p>
        </div>
    )
}
export default Post;

// Class Component
/*
import React from 'react'
class Post extends Comment {
    render(){
        return(
            <div>
                <p>Content from {this.props.name} he int is {this.props.knowledge}</p>
            </div>
        )
    }
}
*/