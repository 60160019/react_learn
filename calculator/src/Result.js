import React from 'react';

const Result = (props) => {
  const result = props.result;
  return (
    <div>
      Result = <input type='number' disabled value={result}></input>
    </div>
  );
};

export default Result;
